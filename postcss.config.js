module.exports = {
  plugins: [
    require('autoprefixer')(),
    require('postcss-simple-vars')(),
    require('postcss-nested')(),
    require('postcss-media-minmax')()
  ]
};
