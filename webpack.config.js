const path = require('path');
const HWP = require('html-webpack-plugin');

module.exports = {
  entry: path.resolve(__dirname, 'app/index.jsx'),
  output: {
    path: path.resolve(__dirname, 'build'),
    publicPath: '/',
    filename: 'bundle.app.[chunkhash:8].js'
  },
  module: {
    rules: [
      {
        test: require.resolve('react'),
        use: [{
          loader: 'imports-loader',
          options: {
            shim: 'es5-shim/es5-shim',
            sham: 'es5-shim/es5-sham'
          }
        }]
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader'
          }
        ]
      },
      {
        test: /\.scss/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            options: {
              modules: {
                localIdentName: '[name]__[local]__[hash:base64:5]'
              },
              importLoaders: 3
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              config: {
                path: 'postcss.config.js'
              }
            }
          },
          {
            loader: 'sass-loader'
          }
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]'
        }
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [
      path.resolve(__dirname, 'app'),
      path.resolve(__dirname, 'node_modules')
    ],
    alias: {
      components: path.resolve(__dirname, 'app/components'),
      constants: path.resolve(__dirname, 'app/constants'),
      config: path.resolve(__dirname, 'app/config'),
      helpers: path.resolve(__dirname, 'app/helpers'),
      node_modules: path.resolve(__dirname, 'node_modules'),
      styles: path.resolve(__dirname, 'app/styles'),
      contexts: path.resolve(__dirname, 'app/contexts'),
      reducers: path.resolve(__dirname, 'app/reducers')
    }
  },
  plugins: [
    new HWP({
      inject: true,
      template: path.resolve(__dirname, 'app/index.html'),
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true
      }
    })
  ],
  devServer: {
    contentBase: './build',
    noInfo: false,
    historyApiFallback: true,
    disableHostCheck: true
  }
};
