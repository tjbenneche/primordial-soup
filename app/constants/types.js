const types = {
  alpha: {
    color: 'blue',
    size: 20,
    visionRadius: 100
  }
};

export default types;
