const typePropertyRanges = {
  size: {
    alpha: .2
  },
  visionRadius: {
    alpha: .3
  }
};

export default typePropertyRanges;
