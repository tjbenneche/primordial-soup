const initialState = {

};

const globalReducer = (state, action) => {
  switch (action.type) {
    case 'PUBLISH_ORGANISM': {
      return { ...state, [action.id]: { ...action.properties } };
    }

    default:
      return state;
  }
};

export { initialState };
export default globalReducer;
