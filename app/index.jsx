import React from 'react';
import ReactDOM from 'react-dom';
import 'core-js';
import 'regenerator-runtime/runtime';

import AppRouter from 'components/AppRouter';

import { GlobalProvider } from 'contexts/GlobalContext';

if (document.getElementById('root')) {
  ReactDOM.render(
    <GlobalProvider>
      <AppRouter />
    </GlobalProvider>
  , document.getElementById('root'));
}
