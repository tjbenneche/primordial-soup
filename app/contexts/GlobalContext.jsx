import React, {
  createContext,
  useContext,
  useReducer
} from 'react';

import globalReducer, { initialState } from 'reducers/globalReducer';

const GlobalContext = createContext();

export const GlobalProvider = ({ children }) => {
  const value = useReducer(globalReducer, initialState);

  return (
    <GlobalContext.Provider value={value}>
      {children}
    </GlobalContext.Provider>
  );
};

export const useGlobalContext = () => useContext(GlobalContext);
