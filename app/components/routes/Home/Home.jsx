import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import Organism from 'components/common/Organism/Organism';

import styles from './Home.scss';

const Home = ({ }) => {
  const canvasRef = useRef();
  const [numAlpha, setNumAlpha] = useState(Array.from(Array(10)))

  useEffect(() => {
    
  }, []);

  return (
    <div className={styles.Canvas} ref={canvasRef}>
      {numAlpha.map((_, i) => (
        <Organism type="alpha" key={i} />
      ))}
    </div>
  );
};

Home.propTypes = {
};

export default Home;
