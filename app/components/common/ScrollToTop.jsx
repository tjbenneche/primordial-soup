import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

function ScrollToTop({
  location,
  children
}) {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [location]);
  return children;
}

ScrollToTop.propTypes = {
  location: PropTypes.shape(null),
  children: PropTypes.node
};


export default withRouter(ScrollToTop);
