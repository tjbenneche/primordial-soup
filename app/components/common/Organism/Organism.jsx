import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import { useGlobalContext } from 'contexts/GlobalContext';
import { types, typePropertyRanges } from 'constants';

import styles from './Organism.scss';

const Organism = ({ type }) => {
  const [top, setTop] = useState(0);
  const [bottom, setBottom] = useState(0);
  const [left, setLeft] = useState(0);
  const [right, setRight] = useState(0);
  const [properties, setProperties] = useState();

  useEffect(() => {
    const directions = ['North', 'South', 'East', 'West'];


  }, []);

  useEffect(() => {
    if (type && types) {
      const { color, size, visionRadius } = types[type];

      const sizeRange = typePropertyRanges.size[type];
      const visionRadiusRange = typePropertyRanges.size[type];

      const sizeAdjustment = Math.random() * sizeRange;
      const visionRadiusAdjustment = Math.random() * visionRadiusRange;

      const sizeAdjustmentDirection = Math.random() < .5 ? -1 : 1;
      const visionRadiusAdjustmentDirection = Math.random() < .5 ? -1 : 1;

      setProperties({
        color,
        size: size + (sizeAdjustment * size * sizeAdjustmentDirection),
        visionRadius: visionRadius + (visionRadiusAdjustment * visionRadius * visionRadiusAdjustmentDirection)
      })
    }
  }, [type, types]);

  const [, dispatch] = useGlobalContext();

  useEffect(() => {
    if (properties) {
      dispatch({ type: 'PUBLISH_ORGANISM', id: Math.floor(Math.random() * 1000), properties });
    }
  }, [properties]);

  return properties ? (
    <div
      className={styles.Organism}
      style={{
        backgroundColor: properties.color,
        width: properties.size,
        height: properties.size,
        top,
        bottom,
        left,
        right
      }}
    >
    </div>
  ) : null;
};

Organism.propTypes = {
};

export default Organism;
