import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Home from 'components/routes/Home/Home';
import ScrollToTop from 'components/common/ScrollToTop';

const AppRouter = () => (
  <Router>
    <ScrollToTop>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
      </Switch>
    </ScrollToTop>
  </Router>
);

export default AppRouter;
